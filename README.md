# Just the Mix

- For HTML/PHP-based projects sans Laravel 
- Sass ready
- Browser-sync ready
- No fancy stuff besides an image re-sizer 

## Get Started
Clone: 
```
git clone https://spinline@bitbucket.org/spinline/mix-boiler.git
```
OR 

Download:  
[https://bitbucket.org/spinline/mix-boiler/downloads/](https://bitbucket.org/spinline/mix-boiler/downloads/)

Then run `npm i` to install dependencies

### For image resizing

1. Place your images[^1] inside **resources/images**
2. Run `npm run prod`

Your images will be exported to **public/img**.

[^1]: If needed, specify your responsive sizes and settings within **webpack.mix.js**


