let mix = require("laravel-mix");

require("laravel-mix-image-resizer");

mix
	.js("resources/js/app.js", "js")
	.sass("resources/scss/app.scss", "css")
	.setPublicPath("public")

	.sourceMaps(true, "source-map")

	.browserSync("http://img-optim.test")

	.disableSuccessNotifications();

if (mix.inProduction()) {
	mix
	.sourceMaps(false)
	.ImageResizer({
		//  disable: process.env.NODE_ENV !== 'production',
		from: "resources/images",
		to: "public/img",
		sizes: [400, 600, 800, 1000, 1200, 1400, 1600, 1800],
		webp: true,
		imageminPngquantOptions: {
			quality: [0.4, 0.7],
		},
		imageminWebpOptions: {
			quality: 80,
		},

		//  imageminJpegtranOptions: {
		// 	 progressive: true
		//  }
	});
}
